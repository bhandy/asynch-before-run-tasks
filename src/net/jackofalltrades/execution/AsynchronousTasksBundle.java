package net.jackofalltrades.execution;

import com.intellij.CommonBundle;
import com.intellij.reference.SoftReference;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

import java.lang.ref.Reference;
import java.util.ResourceBundle;

/**
 * Interface to the resource bundle for the plugin.
 *
 * @author bhandy
 */
public class AsynchronousTasksBundle {

    public static String message(@NotNull @PropertyKey(resourceBundle = BUNDLE) String key, @NotNull Object... arguments) {
        return CommonBundle.message(getBundle(), key, arguments);
    }

    @NonNls
    private static final String BUNDLE = "net.jackofalltrades.execution.AsynchronousTasksBundle";

    private static Reference<ResourceBundle> bundle;

    private static ResourceBundle getBundle() {
        ResourceBundle resourceBundle = SoftReference.dereference(bundle);
        if (resourceBundle == null) {
            resourceBundle = ResourceBundle.getBundle(BUNDLE);
            bundle = new SoftReference<ResourceBundle>(resourceBundle);
        }

        return resourceBundle;
    }

    private AsynchronousTasksBundle() {

    }

}
