package net.jackofalltrades.execution.maven;

import com.intellij.execution.BeforeRunTaskProvider;
import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.runners.ExecutionUtil;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.execution.ParametersListUtil;
import icons.MavenIcons;
import net.jackofalltrades.execution.AsynchronousTasksBundle;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.idea.maven.execution.MavenEditGoalDialog;
import org.jetbrains.idea.maven.execution.MavenRunConfiguration;
import org.jetbrains.idea.maven.execution.MavenRunConfigurationType;
import org.jetbrains.idea.maven.execution.MavenRunnerParameters;
import org.jetbrains.idea.maven.execution.MavenRunnerSettings;
import org.jetbrains.idea.maven.model.MavenExplicitProfiles;
import org.jetbrains.idea.maven.project.MavenGeneralSettings;
import org.jetbrains.idea.maven.project.MavenProject;
import org.jetbrains.idea.maven.project.MavenProjectsManager;

import javax.swing.*;
import java.util.List;

/**
 * <p>Implementation of BeforeRunTaskProvider to support running Maven goals asynchronously in a new Run Tab.</p>
 * <p>Code inspired by IntelliJ's implementation of MavenBeforeRunTasksProvider.</p>
 *
 * @author bhandy
 */
public class MavenAsynchronousBeforeRunTasksProvider extends BeforeRunTaskProvider<MavenAsynchronousBeforeRunTask> {

    public static final Key<MavenAsynchronousBeforeRunTask> ID = Key.create("JOAT.Maven.AsynchronousBeforeRunTask");

    private final Project _project;

    public MavenAsynchronousBeforeRunTasksProvider(Project _project) {
        this._project = _project;
    }

    @Override
    public Key<MavenAsynchronousBeforeRunTask> getId() {
        return ID;
    }

    @Override
    public String getName() {
        return AsynchronousTasksBundle.message("asynchronous.maven.tasks.before.run.empty");
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return MavenIcons.MavenLogo;
    }

    @Override
    public String getDescription(MavenAsynchronousBeforeRunTask task) {
        MavenProject mavenProject = getMavenProject(task);
        if (mavenProject == null) {
            return getName();
        }

        String desc = mavenProject.getDisplayName() + ": " + StringUtil.notNullize(task.getGoal()).trim();
        return AsynchronousTasksBundle.message("asynchronous.maven.tasks.before.run", desc);
    }

    @Override
    public boolean isConfigurable() {
        return true;
    }

    @Nullable
    @Override
    public MavenAsynchronousBeforeRunTask createTask(RunConfiguration runConfiguration) {
        return new MavenAsynchronousBeforeRunTask();
    }

    @Override
    public boolean configureTask(RunConfiguration runConfiguration, MavenAsynchronousBeforeRunTask task) {
        MavenEditGoalDialog dialog = new MavenEditGoalDialog(_project);

        dialog.setTitle(AsynchronousTasksBundle.message("asynchronous.maven.tasks.select.goal.title"));

        if (task.getGoal() == null) {
            MavenProjectsManager projectsManager = MavenProjectsManager.getInstance(_project);
            List<MavenProject> rootProjects = projectsManager.getRootProjects();
            if (rootProjects.size() > 0) {
                dialog.setSelectedMavenProject(rootProjects.get(0));
            } else {
                dialog.setSelectedMavenProject(null);
            }
        } else {
            dialog.setGoals(task.getGoal());
            MavenProject mavenProject = getMavenProject(task);
            if (mavenProject != null) {
                dialog.setSelectedMavenProject(mavenProject);
            } else {
                dialog.setSelectedMavenProject(null);
            }
        }

        if (!dialog.showAndGet()) {
            return false;
        }

        task.setProjectFilePath(AsynchronousTasksBundle.message("maven.project.path", dialog.getWorkDirectory()));
        task.setGoal(dialog.getGoals());
        return true;
    }

    @Override
    public boolean canExecuteTask(RunConfiguration configuration, MavenAsynchronousBeforeRunTask task) {
        return task.getProjectFilePath() != null && task.getGoal() != null;
    }

    @Override
    public boolean executeTask(final DataContext context, final RunConfiguration configuration, ExecutionEnvironment env,
                final MavenAsynchronousBeforeRunTask task) {

        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                Project project = CommonDataKeys.PROJECT.getData(context);
                MavenProject mavenProject = MavenAsynchronousBeforeRunTasksProvider.this.getMavenProject(task);

                if (project == null || project.isDisposed() || mavenProject == null) {
                    return;
                }

                FileDocumentManager.getInstance().saveAllDocuments();

                MavenGeneralSettings settings = null;
                MavenRunnerSettings runnerSettings = null;
                MavenRunnerParameters runnerParameters;
                if (configuration instanceof MavenRunConfiguration) {
                    MavenRunConfiguration taskConfiguration = (MavenRunConfiguration) configuration.clone();
                    settings = taskConfiguration.getGeneralSettings();
                    runnerSettings = taskConfiguration.getRunnerSettings();
                    runnerParameters = taskConfiguration.getRunnerParameters();

                    runnerParameters.setWorkingDirPath(mavenProject.getDirectory());
                    runnerParameters.setGoals(ParametersListUtil.parse(task.getGoal()));
                } else {
                    MavenExplicitProfiles profiles = MavenProjectsManager.getInstance(project).getExplicitProfiles();
                    runnerParameters = new MavenRunnerParameters(true, mavenProject.getDirectory(),
                            ParametersListUtil.parse(task.getGoal()), profiles.getEnabledProfiles(),
                            profiles.getDisabledProfiles());
                }

                RunnerAndConfigurationSettings runConfiguration =
                        MavenRunConfigurationType.createRunnerAndConfigurationSettings(settings, runnerSettings,
                                runnerParameters, project);

                ExecutionUtil.runConfiguration(runConfiguration, DefaultRunExecutor.getRunExecutorInstance());
            }
        });

        return true;
    }

    @Nullable
    private MavenProject getMavenProject(MavenAsynchronousBeforeRunTask task) {
        String pomXmlPath = task.getProjectFilePath();
        if (StringUtil.isEmpty(pomXmlPath)) return null;

        VirtualFile file = LocalFileSystem.getInstance().findFileByPath(pomXmlPath);
        if (file == null) return null;

        return MavenProjectsManager.getInstance(_project).findProject(file);
    }

}
