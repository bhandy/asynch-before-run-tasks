package net.jackofalltrades.execution.maven;

import com.intellij.execution.BeforeRunTask;
import org.jdom.Element;

/**
 * <p>Implementation of BeforeRunTask to represent a Maven goal to be run asynchronously.</p>
 * <p>Code inspired by IntelliJ's implementation of MavenBeforeRunTask.</p>
 *
 *
 * @author bhandy
 */
public class MavenAsynchronousBeforeRunTask extends BeforeRunTask<MavenAsynchronousBeforeRunTask> {

    private String _projectFilePath;
    private String _goal;

    public MavenAsynchronousBeforeRunTask() {
        super(MavenAsynchronousBeforeRunTasksProvider.ID);
    }

    public void setProjectFilePath(String projectFilePath) {
        _projectFilePath = projectFilePath;
    }

    public String getProjectFilePath() {
        return _projectFilePath;
    }

    public void setGoal(String goal) {
        _goal = goal;
    }

    public String getGoal() {
        return _goal;
    }

    @Override
    public void writeExternal(Element element) {
        super.writeExternal(element);
        if (_projectFilePath != null) {
            element.setAttribute("file", _projectFilePath);
        }
        if (_goal != null) {
            element.setAttribute("goal", _goal);
        }
    }

    @Override
    public void readExternal(Element element) {
        super.readExternal(element);
        _projectFilePath = element.getAttributeValue("file");
        _goal = element.getAttributeValue("goal");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MavenAsynchronousBeforeRunTask that = (MavenAsynchronousBeforeRunTask) o;

        if (_projectFilePath != null ? !_projectFilePath.equals(that._projectFilePath) : that._projectFilePath != null)
            return false;
        return _goal != null ? _goal.equals(that._goal) : that._goal == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (_projectFilePath != null ? _projectFilePath.hashCode() : 0);
        result = 31 * result + (_goal != null ? _goal.hashCode() : 0);
        return result;
    }

}
